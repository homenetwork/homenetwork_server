var express = require('express');
var router = express.Router();
var gcm = require('node-gcm');

var server_access_key = 'AIzaSyBHOmal1PKFBZ8F6rVKeSkWQUFiQmbf2ik';
var sender = new gcm.Sender(server_access_key);
var registrationIds = [];

var token = 'dhZOgaNSDKM:APA91bGLg1ugxgC4Upyk1Xltd_F0HsxmU99BuFb1I0VvPC9-e_hMEZNDXPU65iq11suhhUMnMnulfYdTWN098ZzMydzj9Oz-nXmD3akKnuUcA-d2OMsCf3PrWtlduuPixFrSim00nOLy';
registrationIds.push(token);

/*
 상태
 가스 : 누출경고, 정상 보고
 동작 : 동작감지

 데이터
 스위치 : ON/OFF -> power switch
 DIMM : ON/OFF, 조명 레벨 UP/DOWN
 도어락 : 상태 변경
 */

var gasDetection = false;

var power_value = false;
var dimm_value = false;
var dimm_level = 20;
var doorLock_value = false;

/* GET home page. */
router.get('/', function (req, res, next) {
    res.send([{gas: gasDetection}, {power: power_value}, {dimm: dimm_value}, {doorLock: doorLock_value}, {dimmLevel: dimm_level}]);
});

module.exports = router;
/*--------가스--------*/
// 가스 상태 WEB 요청
router.get('/gas', function (req, res, next) {
    res.send({gas: gasDetection});
});

// 가스 누출 GCM 요청
router.get('/gas_abnormal', function (req, res, next) {

    var message = new gcm.Message({
        collapseKey: 'gasDetection',
        delayWhileIdle: true,
        timeToLive: 3,
        data: {
            title: 'gas',
            message: '누출',
            custom_key1: 'gasDetection',
            custom_key2: gasDetection
        }
    });

    sender.send(message, registrationIds, 4, function (err, result) {
        console.log(result);
    });

    gasDetection = true;

    res.send({gas: gasDetection});
});

// 가스 누출 후 정상 GCM 요청
router.get('/gas_normal', function (req, res, next) {
    var message = new gcm.Message({
        collapseKey: 'gasDetection',
        delayWhileIdle: true,
        timeToLive: 3,
        data: {
            title: 'gas',
            message: '정상상태',
            custom_key1: 'gasDetection',
            custom_key2: gasDetection
        }
    });

    sender.send(message, registrationIds, 4, function (err, result) {
        console.log(result);
    });

    gasDetection = false;

    res.send({gas: gasDetection});
});

/*--------동작감지--------*/
// 동작 감지 GCM 요청
router.get('/motionDetection', function (req, res, next) {
    var message = new gcm.Message({
        collapseKey: 'motionDetection',
        delayWhileIdle: true,
        timeToLive: 3,
        data: {
            title: 'motionDetection',
            message: '움직임 감지',
            custom_key1: 'motionDetection',
            custom_key2: 'true'
        }
    });

    sender.send(message, registrationIds, 4, function (err, result) {
        console.log(result);
    });

    res.send({motionDetection: true});
});

/*--------POWER--------*/
// power 상태 WEB 요청
router.get('/power', function (req, res, next) {
    res.send({power: power_value});
});

// power on 상태 GCM 알림
router.get('/power_on', function (req, res, next) {
    if (power_value == false) {
        power_value = true;
        var message = new gcm.Message({
            collapseKey: 'power',
            delayWhileIdle: true,
            timeToLive: 3,
            data: {
                title: 'power',
                message: '파워 ON',
                custom_key1: 'power_value',
                custom_key2: power_value
            }
        });

        sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
    }
    res.send({power: power_value});
});

// power on 요청 Phone to PC
router.get('/power/on', function (req, res, next) {
    if (power_value == false) {
        power_value = true;
    }
    res.send({power: power_value});
});

// power off 상태 GCM 알림
router.get('/power_off', function (req, res, next) {
    if (power_value == true) {
        power_value = false;
        var message = new gcm.Message({
            collapseKey: 'power',
            delayWhileIdle: true,
            timeToLive: 3,
            data: {
                title: 'power',
                message: '파워 OFF',
                custom_key1: 'power_value',
                custom_key2: power_value
            }
        });

        sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
    }
    res.send({power: power_value});
});

// power off 요청 Phone to PC
router.get('/power/off', function (req, res, next) {
    if (power_value == true) {
        power_value = false;
    }
    res.send({power: power_value});
});

/*--------DOOR LOCK--------*/
// doorLock 상태 WEB 요청
router.get('/door', function (req, res, next) {
    res.send({doorLock: doorLock_value});
});

// doorLock on 상태 GCM 알림
router.get('/door_on', function (req, res, next) {
    if (doorLock_value == false) {
        doorLock_value = true;
        var message = new gcm.Message({
            collapseKey: 'door',
            delayWhileIdle: true,
            timeToLive: 3,
            data: {
                title: 'door',
                message: '문 열림',
                custom_key1: 'doorLock_value',
                custom_key2: doorLock_value
            }
        });

        sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
    }

    res.send({door: doorLock_value});
});

// doorLock on 요청 Phone to PC
router.get('/door/on', function (req, res, next) {
    if (doorLock_value == false) {
        doorLock_value = true;
    }

    res.send({door: doorLock_value});
});

// doorLock off 상태 GCM 알림
router.get('/door_off', function (req, res, next) {
    if (doorLock_value == true) {
        doorLock_value = false;
        var message = new gcm.Message({
            collapseKey: 'door',
            delayWhileIdle: true,
            timeToLive: 3,
            data: {
                title: 'door',
                message: '문 닫힘',
                custom_key1: 'doorLock_value',
                custom_key2: doorLock_value
            }
        });

        sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
    }

    res.send({door: doorLock_value});
});

// doorLock on 요청 Phone to PC
router.get('/door/off', function (req, res, next) {
    if (doorLock_value == true) {
        doorLock_value = false;
    }

    res.send({door: doorLock_value});
});

/*--------dimm--------*/
// dimm 상태 WEB 요청
router.get('/dimm', function (req, res, next) {
    res.send({dimm: dimm_value});
});

// dimm on 상태 GCM 알림
router.get('/dimm_on', function (req, res, next) {
    if (dimm_value == false) {
        dimm_value = true;
        dimm_level = 1;
        var message = new gcm.Message({
            collapseKey: 'dimm',
            delayWhileIdle: true,
            timeToLive: 3,
            data: {
                title: 'dimm',
                message: '형광등 켜짐',
                custom_key1: 'dimm_value',
                custom_key2: dimm_value
            }
        });

        sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
    }

    res.send({dimm: dimm_value});
});

// dimm on 요청 Phone to PC
router.get('/dimm/on', function (req, res, next) {
    if (dimm_value == false) {
        dimm_value = true;
        dimm_level = 1;
    }

    res.send({dimm: dimm_value});
});

// dimm off 상태 GCM 알림
router.get('/dimm_off', function (req, res, next) {
    if (dimm_value == true) {
        dimm_value = false;
        dimm_level = 20;
        var message = new gcm.Message({
            collapseKey: 'dimm',
            delayWhileIdle: true,
            timeToLive: 3,
            data: {
                title: 'dimm',
                message: '형광등 꺼짐',
                custom_key1: 'dimm_value',
                custom_key2: dimm_value
            }
        });

        sender.send(message, registrationIds, 4, function (err, result) {
            console.log(result);
        });
    }

    res.send({dimm: dimm_value});
});

// dimm off 요청 Phone to PC
router.get('/dimm/off', function (req, res, next) {
    if (dimm_value == true) {
        dimm_value = false;
        dimm_level = 20;
    }

    res.send({dimm: dimm_value});
});

// dimm up 요청 Phone to PC
router.get('/dimm/up', function (req, res, next) {
    if (dimm_level > 1)
        dimm_level--;

    res.send({dimmLevel: dimm_level});
});

// dimm down 요청 Phone to PC
router.get('/dimm/down', function (req, res, next) {
    if (dimm_level < 20)
        dimm_level++;

    res.send({dimmLevel: dimm_level});
});

