# 마이크로프로세서 과제용 RestFul

* / : 전체 상태 확인

* door : 문 상태 확인
      * door_on : 문열림
      * door_off : 문닫힘
      * door/on : 문열림 요청
      * door/off : 문닫힘 요청

* gas :  가스 상태 확인
      * gas_on : 가스 열림 
      * gas_off : 가스 닫힘
      * gas/on : 가스 열림 요청
      * gas/off : 가스 닫힘 요청

* switch :  스위치 상태 확인
      * switch_on : 스위치 열림 
      * switch_off : 스위치 닫힘
      * switch/on : 스위치 열림 요청
      * switch/off : 스위치 닫힘 요청

* light :  전등 상태 확인
      * light_on : 전등 켜짐
      * light_off : 전등 꺼짐
      * light/on : 전등 켜짐 요청
      * light/off : 전등 꺼짐 요청